<?php
defined('ABSPATH') || die('Not allowed');

class LibraryMultiSearch {


	static function doSearchBox($atts=[]){

		$atts = shortcode_atts(array(
			'styles' => 'on'
		),$atts);

		if( self::getOption('use-styles') && $atts['styles'] != 'off' ) {			wp_enqueue_style ('library-multi-search', LIBRARY_MULTI_SEARCH_URL . 'assets/library-multi-search.css', array(), LIBRARY_MULTI_SEARCH_VERSION);
		}

		wp_enqueue_script('library-multi-search', LIBRARY_MULTI_SEARCH_URL . 'assets/library-multi-search.js', array('jquery'), LIBRARY_MULTI_SEARCH_VERSION);

		$typesToShow = self::getOption('types-to-display');
		$scopes = self::getOption('library-scopes');
		$searchScopeName = (self::getOption('catalog-provider') == 'iii')
			? 'searchscope'
			: 'lm';
		$lmsJsArgs = array(
			'types' => $typesToShow,
			'hideOptionsUntilFocus' => self::getOption('hide-options-until-focus'),
			'homeurl' => home_url(),
		);

		if( in_array('catalog', $typesToShow) ) {
			$lmsJsArgs['catalogBase'] = self::getOption('catalog-base');
			$lmsJsArgs['catalogProvider'] = self::getOption('catalog-provider');
			if( count($scopes) == 1 )
				$lmsJsArgs['catalogSingleScope'] = array(
					'name' => $searchScopeName,
					'value' => $scopes[0]['scope']
				);
		}

		if( in_array('ebooks', $typesToShow) ) {
			$lmsJsArgs['ebookProvider'] = self::getOption('ebook-provider');
			$lmsJsArgs['ebookProviderUrl'] = self::getOption('ebook-provider-url');
		}

		wp_localize_script( 'library-multi-search','lmsArgs', $lmsJsArgs );

		if( count($scopes) > 1 ) {

			$searchplaces = '';
			foreach($scopes as $group) {
				$id = "cat-scope-" . preg_replace("/[^A-Za-z0-9]/",'',$group['name']);
				$searchplaces .= '<label>
					<input type="radio" name="'.$searchScopeName.'" value="'.$group['scope'].'" id="'.$id.'" checked> 
					'. $group['name'] .'</label>
				';
			}
		}

		$openResultsInNewWindow = (self::getOption('open-results-in-new-window'))
			? ' target="_blank" '
			: '';

		$formClasses = array();
		if( self::getOption('hide-options-until-focus') )
			$formClasses[] = 'hide-options-until-focus';
		$formClasses = implode(' ', $formClasses);

		$out = '
		<form action="" method="GET" id="multi-search" '.$openResultsInNewWindow.' class="'.$formClasses.'">';
		if( self::getOption('display-title', true) )
			$out .= '<h2>Search</h2>';



		$out .= '<input type="text" value="" name="s" class="main-search-input" placeholder="'.esc_attr(self::getOption('input-placeholder')).'"> 
			<input type="submit" value="Go" name="submit">
			<div class="lms-search-options">
		';
		if( count($typesToShow) > 1 ) {
			$out .= '<div class="choose-search-wrap lms-search-subsection">';

			if( in_array('catalog', $typesToShow) ) {
				$out .= '<label for="option-catalog">
					<input selected="selected" type="radio" name="searchsite" value="catalog" id="option-catalog">
					Catalog
				</label>';
			}
			if( in_array('website', $typesToShow) ) {
				$out .= '<label for="option-site">
					<input type="radio" name="searchsite" value="site" id="option-site">
					Website
				</label>';
			}
			if( in_array('ebooks', $typesToShow) ) {
				$out .= '<label for="option-ebooks">
					<input type="radio" name="searchsite" value="ebooks" id="option-ebooks">
					eBooks
				</label>';
			}
			
			$out .= '</div>';
		}
			
		if( in_array('catalog', $typesToShow) ) {
			$out .= '<div class="searchtype-options lms-search-subsection" data-searchsite="catalog">
				<div class="filter-group">
					<span class="filter-group-label">Search In</span>';
			if( self::getOption('catalog-provider') == 'iii' ) :
				$out .= '
				<label for="cat-keyword">
				<input type="radio" name="customt" value="X" id="cat-keyword" checked> 
				Keyword</label>

				<label for="cat-author">
				<input type="radio" name="customt" value="a" id="cat-author"> 
				Author</label>
				
				<label for="cat-title">
				<input type="radio" name="customt" value="t" id="cat-title"> 
				Title</label>
					';
			else :
				$out .= '
				<label for="cat-keyword">
				<input type="radio" name="rt" value="" id="cat-keyword" checked> 
				Keyword</label>
				
				<label for="cat-author">
				<input type="radio" name="rt" value="false|||AUTHOR|||Author" id="cat-author">
				Author</label>
				
				<label for="cat-title">
				<input type="radio" name="rt" value="false|||TITLE|||Title" id="cat-title">
				Title</label>
				';
			endif;

			$out .= '</div>';

			$materialTypeLimiters = self::getOption('material-type-limiters');
			if( $materialTypeLimiters ) {
				$out .= '<div class="filter-group">
					<span class="filter-group-label">Search only</span><span class="filter-group-options">';
				switch(self::getOption('catalog-provider')) {
					case 'sirsi' :
						$materialTypeLimiterFieldname = 'qf[]';
						$bookVal = 'BOOK'."\t".'Book';
						$audiobookVal = 'AUDIOBOOK CD'."\t".'Audiobook CD';
						$ebookLimiterValue = 'E_BOOK'."\t".'eBook';
						$dvdLimiterValue = 'DVD'."\t".'DVD';
						$cdLimiterValue = 'CD'."\t".'CD';
						break;
					case 'iii' :
						$materialTypeLimiterFieldname = 'm';
						$bookVal = 'a';
						$audiobookVal = '3';
						$ebookLimiterValue = 'x';
						$dvdLimiterValue = '5';
						$cdLimiterValue = '1';
						break;
				}

				if( in_array('book', $materialTypeLimiters) ) {
					$out .= '<label>
						<input type="checkbox" name="'.$materialTypeLimiterFieldname.'" value="'.$bookVal.'">
						Book
					</label>';
				}
				if( in_array('audiobook', $materialTypeLimiters) ) {
					$out .= '<label>
						<input type="checkbox" name="'.$materialTypeLimiterFieldname.'" value="'.$audiobookVal.'">
						Audiobook
					</label>';
				}
				if( in_array('ebook', $materialTypeLimiters) ) {	
					$out .= '<label>
						<input type="checkbox" name="'.$materialTypeLimiterFieldname.'" value="'.$ebookLimiterValue.'">
						eBook
					</label>';
				}
				if( in_array('dvd', $materialTypeLimiters) ) {	
					$out .= '<label>
						<input type="checkbox" name="'.$materialTypeLimiterFieldname.'" value="'.$dvdLimiterValue.'">
						DVD
					</label>';
				}
				if( in_array('cd', $materialTypeLimiters) ) {	
					$out .= '<label>
						<input type="checkbox" name="'.$materialTypeLimiterFieldname.'" value="'.$cdLimiterValue.'">
						CD
					</label>';
				}

				$out .= '</span></div>';
			}

			if( self::getOption('use-currently-available-limiter') && self::getOption('catalog-provider') == 'iii' ) {
				$out .= '<div class="filter-group">
					<span class="filter-group-label">Availability</span>
					<label for="availlim">
					<input type="checkbox" name="availlim" value="1" id="availlim">
					 Only show currently available
					 </label>
					</div>
				';
			}



			if( count($scopes) > 1 ) {
				$out .= '
					<div class="filter-group">
						<span class="filter-group-label">Where</span>
						<span class="filter-group-options">'.$searchplaces.'</span>
					</div>';
			}
			$out .= '</div>';
		}

		if( in_array('ebooks', $typesToShow) && self::getOption('ebook-provider') == 'sirsi' ) {
			$out .= '<div class="searchtype-options lms-search-subsection" data-searchsite="ebooks">
				<div class="filter-group">
					<span class="filter-group-label">Type</span> <span class="filter-group-options">
				<label for="ebook-keyword">
				<input type="radio" name="rt" value="" id="ebook-keyword" checked> 
				Keyword</label>
				
				<label for="ebook-author">
				<input type="radio" name="rt" value="false|||AUTHOR|||Author" id="ebook-author">
				Author</label>
				
				<label for="ebook-title">
				<input type="radio" name="rt" value="false|||TITLE|||Title" id="ebook-title">
				Title</label>
				</span>
				';

			$out .= '</div>';
			if( count($scopes) > 1 ) {
				$out .= '
					<div class="filter-group">
						<p>Where</p>
						'.$searchplaces.'
					</div>';
			}
			$out .= '</div>';
		}
		
		if( self::getOption('hide-options-until-focus') )
			$out .= '<a class="hide-search-options js-hide-search-options" href="#">Hide</a>';

		$out .= '
			</div><!-- .lms-search-options -->
		</form>
		';

		return $out;

	}

	static private function getOptionDefaults(){
		return array(
			'library-scopes' => array(),
			'ebook-provider' => '',
			'ebook-provider-url' => '',
			'catalog-base' => '',
			'catalog-provider' => 'iii',
			'use-currently-available-limiter' => false,
			'types-to-display' => array(
				'website',
				'catalog',
				'ebooks'
			),
			'material-type-limiters' => array(
				'book',
				'audiobook',
				'ebook',
				'dvd',
				'cd',
			),
			'use-styles' => true,
			'open-results-in-new-window' => false,
			'display-title' => true,
			'input-placeholder' => '',
			'hide-options-until-focus' => false,
		);
	}

	static function getOption($field, $default=false){
		$opts = self::get_options();
		if( isset($opts[$field]) )
			return $opts[$field];
		else
			return $default;
	}

	static function get_options() {
		static $opts = null;
		if( is_null($opts) )
			$opts = get_option('libraryMultiSearch_settings', self::getOptionDefaults() );
		return $opts;
	}

	static function set_options($arr){
		$opts = array_merge(
			self::getOptionDefaults(),
			$arr
		);
		update_option('libraryMultiSearch_settings', $opts);
	}


}