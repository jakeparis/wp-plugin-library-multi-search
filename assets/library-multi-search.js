jQuery(function(){
		
	var options = {},
		args = window.lmsArgs;
	
	var isSmallScreen = function() { 
		return jQuery(window).width() < 600;
	};

	if( args.types.indexOf('catalog') > -1 ) {
		if( args.catalogProvider === 'iii' ) {
			if( isSmallScreen() ){
				args.catalogBase = args.catalogBase.replace('minerva','m.minerva');
				args.catalogBase = args.catalogBase.replace('/X','/Y');
			}
			options.catalog = {
				fields: [			
					{
						"name":"SORT",
						"value":"D"
					}
				],
				mainInputName: "SEARCH",
				method: "get",
				action: function(f){
					var type = f.querySelector('input[name="customt"]:checked');
					if( isSmallScreen() )
						return args.catalogBase + '/search/Y';
					else if ( type )
						return args.catalogBase + '/search/' + type.value;
					else
						return args.catalogBase + '/search/X';		
				},
				eventHandlers: {
					change: function(me,e){
						// limiting by material type doesn't work with a or t search
						if( e.target.name == 'customt' ) {

							me.setAttribute('action', args.catalogBase + '/search/' + e.target.value );

							if( e.target.value != 'X' )
								jQuery('input[name="m"]').parents('.filter-group').eq(0).slideUp('fast');
							else
								jQuery('input[name="m"]').parents('.filter-group').eq(0).slideDown('fast');
						}
					},
				},
			};
		} else if( args.catalogProvider === 'sirsi' ) {
			options.catalog = {
				fields: [
					{
						name: "te",
						value: ""
					},
				],
				mainInputName: "qu",
				method: "get",
				action:args.catalogBase,
				eventHandlers: {
					submit: function(me,e){
						var filters = jQuery(me).find('input[name="qf[]"]:checked');
						if( filters.length < 1 || jQuery(me).find('input[name="qf"]').length)
							return;
						var values = [];
						filters.each( function(){
							values.push( this.getAttribute('value') );
							// this.parentNode.removeChild(this);
							this.disabled = true; // so it doesn't submit
						});
						var qf = 'FORMAT\tFormat\t' + values.join(' || ');
						var inp = document.createElement('input');
						inp.setAttribute('type','hidden');
						inp.setAttribute('name','qf');
						inp.setAttribute('value',qf);
						me.appendChild(inp);
					}
				}
			}
		}

		if( typeof args.catalogSingleScope !== 'undefined' ){
			options.catalog.fields.push( args.catalogSingleScope );
		}

	}
	if( args.types.indexOf('website') > -1 ) {
		options.site = {
			fields: [
				
			],
			"mainInputName": "s",
			"method": "get",
			"action": args.homeurl,
		};
	}

	if( args.types.indexOf('ebooks') > -1 ) {
		switch( args.ebookProvider ) {
			case '3m' :
				options.ebooks = {
					"fields": [],
					"action": args.ebookProviderUrl,
					"method": "get",
					"mainInputName": "s",
					"eventHandlers": {
						submit: function(me,e){
							var val = jQuery(me).find('input[name="s"]').val();
							val = val.replace(/[&"><']/g,'');
							jQuery(me).attr('action', options.ebooks.action + val);
						}
					}
				};
				break;
			case 'overdrive' :
				options.ebooks = {
					fields: [
						{
							"name":"Sort",
							"value":"SortBy=Relevancy"
						},{
							"name":"FullTextField",
							"value":"All"
						},{
							"name":"Type",
							"value": "FullText"
						},{
							"name":"PerPage",
							"value":"24"
						},{
							"name":"URL",
							"value": "SearchResults.htm"
						}
					],
					"mainInputName": "query",
					"method": "get",
					"action": args.ebookProviderUrl
				};
				break;
			case 'sirsi' :
				options.ebooks = {
					fields: [{
						name: "te",
						value: "ERC_ST_BLD",
					}],
					mainInputName: "qu",
					method: "get",
					action: args.ebookProviderUrl
				};
				break;
		}
	}
	
	var setupForm = function(input){
		if(typeof input==='undefined')
			input = document.querySelector('#multi-search input[name="searchsite"]');
		if( ! input )
			return false;
		var name = input.getAttribute('value'),
			o = options[name],
			f = document.getElementById('multi-search')
		;
		var hidden = f.querySelectorAll('input[type="hidden"]');
		hidden.forEach( function(input) {
			input.parentNode.removeChild( input );
		});
		for(var i=0;i<o.fields.length;i++) {
			var inputField = document.createElement('input');
			inputField.setAttribute('type', 'hidden');
			inputField.setAttribute('name', o.fields[i].name );
			inputField.setAttribute('value', o.fields[i].value );
			f.appendChild( inputField );
		}
		f.setAttribute('method', o.method);
		var action;
		if( typeof o.action == 'function' )
			action = o.action(f);
		else
			action = o.action;
		f.setAttribute('action', action);
		f.querySelector('input[type="text"]').setAttribute('name', o.mainInputName);

		// take off previous handlers (namespaced)
		jQuery(f).off('.lmsEvents');

		// add any event handlers
		if( typeof o.eventHandlers != 'undefined' ){
			for(var evName in o.eventHandlers) {
				if( ! o.eventHandlers.hasOwnProperty(evName) ) continue;

				if( typeof o.eventHandlers[evName] == 'function' )
					jQuery(f).on( evName+'.lmsEvents' , function(e){
						o.eventHandlers[evName](this,e); 
					});
			}
		}
	};
	
	var hiliteLabel = function( input ) {
		if( typeof input == 'undefined')
			input = document.querySelector('.choose-search-wrap input[type="radio"]');
			input.checked = true;

		var	$me = jQuery(input),
			$myLabel = $me.parent('label'),
			myValue = input.getAttribute('value'),
			$myOptions = jQuery('.searchtype-options[data-searchsite="'+myValue+'"]')
		;

		// remove selected class from sibling labels
		$me.parents('.choose-search-wrap').eq(0).find('label').not($myLabel).each(function(){
			jQuery(this).removeClass('option-selected');
			this.querySelectorAll('input').forEach(function(input){
				delete input.checked;
			});
		});
		if($myOptions.length) {
			jQuery('.searchtype-options').not($myOptions).each(function(){
				jQuery(this).slideUp(150);
				this.querySelectorAll('input').forEach(function(input){
					input.disabled = true;
				});
			});
			$myOptions.slideDown(150);
			$myOptions.find('input').removeAttr('disabled');

		} else {
			jQuery('.searchtype-options').slideUp(150)
				.find('input').attr('disabled','true');
		}

		$myLabel.addClass('option-selected');
	};
	
	setupForm();
	hiliteLabel();

	var inputs = document.querySelectorAll('.choose-search-wrap input');
	for(var i=0;i<inputs.length;i++){
		inputs[i].addEventListener('change', function(){
			hiliteLabel( this );
			setupForm( this );
		});
	}

	(function(){
		if( ! args.hideOptionsUntilFocus ) 
			return;

		var hideSubsections = function(){
			jQuery('.lms-search-options').slideUp('fast');
		};
		var showSubsections = function(){
			jQuery('.lms-search-options').slideDown('fast');
		}

		var search = document.querySelector('#multi-search .main-search-input');
		search.addEventListener('focus', function(){
			showSubsections();
		});

		var hider = document.querySelector('.js-hide-search-options');
		if( hider ) {
			hider.addEventListener('click',function(e){
				e.preventDefault();
				hideSubsections();
			})
		}
		
	})();
	
});