<?php
/*
Plugin Name: Library Multi-Search
Description: Adds a function and shortcode for a multi-search form.
Version: 2.2.0
Author: Jake Paris
Author URI: https://jake.paris
*/

define('LIBRARY_MULTI_SEARCH_VERSION', '2.2.0' );
define('LIBRARY_MULTI_SEARCH_URL', plugins_url('/',__FILE__));


foreach( glob( plugin_dir_path(__FILE__) . 'class/*.php') as $file) {
	include_once $file;
}

require_once plugin_dir_path( __FILE__ ) . 'verification/verify.php';


add_action('admin_menu',function(){
	add_submenu_page( 'options-general.php', 'Multi-Search Form', 'Multi-Search Form', 'edit_posts', 'multi-search-form-settings', 'jp_settings_page' );
});
function jp_settings_page() {
	if( JP_LMS_Verification::isFullyVerified(true) )
		require 'admin-settings.php';
	else
		require 'verification/adminpage-verification.php';
}


/** 
 * [library-multi-search-form]
 *
 * to turn off built-in styles (use your own) add attribute styles="off"
 */
add_action('init',function(){
	if( ! JP_LMS_Verification::isFullyVerified() )
		return;
	add_shortcode('library-multi-search-form', array('LibraryMultiSearch', 'doSearchBox') );
});


/**
 * Updater
 */
require plugin_dir_path(__FILE__) . 'updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/wp-plugin-library-multi-search/',
	__FILE__, //Full path to the main plugin file or functions.php.
	'library-multi-search/library-multi-search.php'
);


register_deactivation_hook( __FILE__, function(){

	JP_LMS_Verification::setKey(false);

});