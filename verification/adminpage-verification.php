<?php
defined('ABSPATH') || die('Not allowed');
?>

<div class="wrap">

<h1>Library Multi Search</h1>
<?php
if( isset($_POST['submit-verification']) ){
	if( ! wp_verify_nonce( $_POST['_lms-license-nonce'], 'lms-license-update' ) ){
		echo '<div class="updated error"><p>There was a problem. Perhaps the form timed out.</p></div>';
	} else {

		if( ! $_POST['lms-license'] ) {
			echo '<div class="updated error"><p>Please enter a license.</p></div>';
		} else {
			$result = JP_LMS_Verification::verifyLicense( $_POST['lms-license'] );

			if( ! $result ){
				echo '<div class="updated error"><p>That license is not valid.</p></div>';
			} else {
				echo '<div class="updated"><p>Thank you! The plugin has been validated. <a href="">Click here to start setting up</a>.</p></div>';

			}
		}
	}
}	

$lmsLicense = JP_LMS_Verification::getLicense();

?>
<p>In order to use this plugin, you will need to enter a license key. Please <a href="https://jakeparis.com/get-in-touch/">contact us</a> to get one.</p>
<form method="post" action="">
	<?php wp_nonce_field( 'lms-license-update', '_lms-license-nonce' ); ?>

	<h3>License</h3>
	<input type="text" name="lms-license" value="<?= $lmsLicense ?>">

	<?php submit_button('Verify','primary','submit-verification') ?>

</form>