# Library Multi-Search #


Adds a multi-search form to search a library catalog.

## Description ##

Adds a function and shortcode for a multi-search form to search library catalogs. Options for Sirsi and III catalogs.

## Changelog ##

### 2.3.0

Updated my website url

### 2.2.0

A few minor updates for accessibility on the form.

### 2.1.7 ###

Updated internal classnames.

### 2.1.6 ###
Updated "Tested up to"

### 2.1.5 ###
Housekeeping; no user-facing changes.

### 2.1.4 ###
Updated updater library.

### 2.1.3 ###
Updated verification library to allow use in multiple plugins.

### 2.1.2 ###

* Fixed form loading js error 

### 2.1.1 ###

* Fixed bug when searching website from not-the-homepage

### 2.1.0 ###

* Added option (for iii) for limiting to currently available items
* Added option (for iii) to limit by material type 
* Added an option to hide/show the Search box title. 
* Added an option to configure placeholder text for the search input.
* Added the ability to hide all options until search box is focused.
* Fixed a bug when a checkbox option is false, but it's default value is true.
* Fixed bug when searching website from not-the-homepage

### 2.0.2 ###
Added default parameter for getOption method. 

### 2.0.1 ###
Add option to open form submission in new window

### 2.0.0 ###
Enhances flexibility in settings and adds ability to select other types
of library interfaces. 

### 1.0.0 ###
Add functionality to search 3M eBook provider.

### 0.9.0 ###
Provides a basic search functionality of Minerva Library System
