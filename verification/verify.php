<?php
defined('ABSPATH') || die('Not allowed');

if( class_exists('JP_LMS_Verification') )
	return;

class JP_LMS_Verification {

	static $pluginBase = 'library-multi-search';

	static function isFullyVerified($reverify=0){
		$lic = self::getLicense();
		$key = self::getKey();
		if( ! $lic || ! $key )
			return false;
		if(!$reverify)
			return true;
		$verified = self::verifyKey();
		if( ! $verified ) {
			self::setKey(false);
			delete_transient( self::settingName('verified') );
		} else {
			set_transient( self::settingName('verified'), '1', WEEK_IN_SECONDS );
		}
		return $verified;
	}

	static function getKey(){
		return get_option( self::settingName('key') );
	}
	static function setKey($key){
		if( $key )
			update_option( self::settingName('key'), $key );
		else
			delete_option( self::settingName('key') );

		delete_transient( self::settingName('verified') );
	}
	static function getLicense(){
		return get_option( self::settingName('license') );
	}
	static function setLicense($lic){
		if( $lic )
			update_option( self::settingName('license'), $lic );
		else
			delete_option( self::settingName('license') );

		delete_transient( self::settingName('verified') );
	}

	private static function settingName($key=''){
		return self::$pluginBase . "_{$key}";
	}

	private static function verifyKey() {
		$lic = self::getLicense();
		$key = self::getKey();
		if( ! $lic || ! $key )
			return false;
		return(md5(self::$pluginBase.parse_url(home_url(), PHP_URL_HOST).$key)==$lic);
	}

	static function verifyLicense($lic){
		$resp = wp_remote_post('http://jakeparis.com/wp-license-verification/', array(
			'body' => array(
				'domain' => home_url(),
				'plugin' => self::$pluginBase,
				'license' => $lic
			)
		));
		$out = wp_remote_retrieve_body( $resp );
		if( ! $out )
			return false;
		$json = json_decode( $out, true);

		if( ! isset($json['validation']) )
			return false;

		self::setLicense( $lic );
		self::setKey( $json['validation'] );
		return true;
	}


}