<?php
defined('ABSPATH') || die('Not allowed');
?>

<style>
	label.block {
		display:  block;
		font-weight: bold;
		margin:  .7em 0 .2em;
	}
</style>
<div class="wrap">

<?php
if( isset($_POST['lms-settings']) ) {

	if( ! wp_verify_nonce( $_POST['_lms-settings-nonce'], 'save-lms-settings' ) )
		echo '<div class="updated error"><p>There was a problem saving the settings. Perhaps the form timed out?</p></div>';
	else {

		$libraryScopes = array();
		foreach($_POST['lib_name'] as $k => $name) {
			if($name=='' &&	 $_POST['lib_scope'][$k] == '')
				continue;
			$libraryScopes[$k] = array(
				'name' => $name,
				'scope' => $_POST['lib_scope'][$k]
			);
		}

		LibraryMultiSearch::set_options([
			'library-scopes' => $libraryScopes,
			'ebook-provider' => $_POST['ebook-provider'],
			'ebook-provider-url' => $_POST['ebook-provider-url'],
			'catalog-base' => $_POST['catalog-base'],
			'catalog-provider' => $_POST['catalog-provider'],
			'use-currently-available-limiter' => isset($_POST['use-currently-available-limiter']),
			'types-to-display' => $_POST['show-search'],
			'material-type-limiters' => $_POST['limiters'],
			'use-styles' => isset($_POST['use-styles']),
			'open-results-in-new-window' => isset($_POST['open-results-in-new-window']),
			'display-title' => isset($_POST['display-title']),
			'input-placeholder' => stripslashes($_POST['input-placeholder']),
			'hide-options-until-focus' => isset($_POST['hide-options-until-focus']),
			'use-currently-available-limiter' => isset($_POST['use-currently-available-limiter']),
		]);

		echo '<div class="updated notice is-dismissible"><p>Saved settings.</p></div>';
	}
}


switch( LibraryMultiSearch::getOption('ebook-provider') ) {
	case '3m' :
		$ebookProvider3m = ' checked ';
		break;
	case 'overdrive' :
		$ebookProviderOverdrive = ' checked ';
		break;
	case 'sirsi' :
		$ebookProviderSirsi = ' checked ';
		break;
}
switch( LibraryMultiSearch::getOption('catalog-provider') ) {
	case 'sirsi' :
		$catalogProviderSirsiChecked = ' checked ';
		break;
	case 'iii' :
		$catalogProviderIIIChecked = ' checked ';
}
$eBookUrl = LibraryMultiSearch::getOption('ebook-provider-url');
$libraryScopes = LibraryMultiSearch::getOption('library-scopes');
$catalogBase = LibraryMultiSearch::getOption('catalog-base');
$typesToDisplay = LibraryMultiSearch::getOption('types-to-display');
$inputPlaceholder = LibraryMultiSearch::getOption('input-placeholder');
$useStylesChecked = (LibraryMultiSearch::getOption('use-styles')) ? ' checked ' : '';
$openResultsInNewWindowChecked = (LibraryMultiSearch::getOption('open-results-in-new-window')) ? ' checked ' : '';
$displayTitleChecked = (LibraryMultiSearch::getOption('display-title',true)) ? ' checked ' : '';
$hideOptionsUntilFocusChecked = (LibraryMultiSearch::getOption('hide-options-until-focus')) ? ' checked ' : '';
$useCurrentlyAvailableLimiterChecked = (LibraryMultiSearch::getOption('use-currently-available-limiter')) ? ' checked ' : '';

if( in_array('website', $typesToDisplay) )
	$showSearchWebsiteChecked = ' checked ';
if( in_array('catalog', $typesToDisplay) )
	$showSearchCatalogChecked = ' checked ';
if( in_array('ebooks', $typesToDisplay) )
	$showSearchEbooksChecked = ' checked ';
?>

<h1>Multi-Search Form Settings</h1>

<p>You may place the search form in one of two ways.</p>
<ol>
	<li>In a post/page editor, you can use the shortcode <b>[library-multi-search-form]</b>
	<li>In a template, you can use the php function <b>LibraryMultiSearch::doSearchBox()</b>
</ol>

<form action="" method="post">

	<?php wp_nonce_field( 'save-lms-settings', '_lms-settings-nonce' ) ?>

	<h3>Which search types to show?</h3>

	<p>
	<input type="checkbox" id="show-search-website" name="show-search[]" value="website" <?= $showSearchWebsiteChecked ?>>
	<label for="show-search-website"> Website search</label>
	</p>

	<p>
	<input type="checkbox" id="show-search-catalog" name="show-search[]" value="catalog" <?= $showSearchCatalogChecked ?>>
	<label for="show-search-catalog"> Catalog search</label>
	</p>

	<p>
	<input type="checkbox" id="show-search-eBooks" name="show-search[]" value="ebooks" <?= $showSearchEbooksChecked ?>>
	<label for="show-search-eBooks"> eBooks search</label>
	</p>

	<hr>

	<h3>Library Catalog Service</h3>
	<p>Select which company provides your library's catalog.</p>
	
	<p>
		<input type="radio" name="catalog-provider" value="iii" id="catalog-provider-iii" <?= $catalogProviderIIIChecked ?>>
		<label for="catalog-provider-iii"> III / Millenium / Sierra</label>
		<br>
		<input type="radio" name="catalog-provider" value="sirsi" id="radio-catalog-provider-sirsi" <?= $catalogProviderSirsiChecked ?>> 
		<label for="radio-catalog-provider-sirsi">Sirsi</label>
	</p>

	<h3>Catalog Base Url</h3>
	<p>Enter the base url for your catalog.</p>
	<input type="text" name="catalog-base" value="<?= esc_attr($catalogBase) ?>">


	<h3>Which libraries/scopes to show in the form.</h3>
	<p>If you only enter one option, it will not show, but will be used when
	the search is performed.</p>

	<table>
		<tr>
			<th>Library Name (label)</th>
			<td>Library Search Scope</td>
		</tr>
	<?php
	$i=0; 
	if( $libraryScopes ) :
		foreach($libraryScopes as $group) {

			echo '<tr>
				<td>
					<input type="text" value="'.$group['name'].'" name="lib_name['.$i.']">
				</td>
				<td>
					<input type="text" value="'.$group['scope'].'" name="lib_scope['.$i.']">
				</td>
			</tr>';
			$i++;
		}
	endif;
	while($i<5){
		echo '<tr>
			<td>
				<input type="text" value="" name="lib_name['.$i.']">
			</td>
			<td>
				<input type="text" value="" name="lib_scope['.$i.']">
			</td>
		</tr>';
		$i++;
	}
	?>
	</table>

	<h3>What Limiters to display</h3>
	<h4>Material Type</h4>
	<?php
	$limiters = LibraryMultiSearch::getOption('material-type-limiters', array());
	foreach(array('book','audiobook','ebook','dvd','cd') as $type){
		$checked = in_array($type, $limiters) ? ' checked ' : '';
		echo '<label for="limiters-'.$type.'">
			<input id="limiters-'.$type.'" type="checkbox" name="limiters[]" value="'.$type.'" '.$checked.'> '.ucfirst($type).'</label>';
	}
	?>

	<h4>Currently Available (only for III/Millenium/Sierra catalog)</h4>
	<input type="checkbox" name="use-currently-available-limiter" id="use-currently-available-limiter" value="1" <?= $useCurrentlyAvailableLimiterChecked ?>>
	<label for="use-currently-available-limiter"> Display an option to limit the search to only available items</label>

	<hr>
	<h3>eBook Provider</h3>

	<input type="radio" <?= $ebookProviderOverdrive ?> name="ebook-provider" value="overdrive" id="ebook-provider-overdrive"> 
	<label for="ebook-provider-overdrive">Overdrive</label>
	<br>
	<input type="radio" <?= $ebookProvider3m ?> name="ebook-provider" value="3m" id="ebook-provider-3m"> 
	<label for="ebook-provider-3m">3M/Bilbiotheca</label>
	<br>
	<input type="radio" <?= $ebookProviderSirsi ?> name="ebook-provider" value="sirsi" id="ebook-provider-sirsi">
	<label for="ebook-provider-sirsi">Sirsi</label>

	<h3>eBook Base URL</h3>

	<input type="text" name="ebook-provider-url" value="<?= esc_attr($eBookUrl) ?>"> 

	<hr>
	<h3>Miscellaneous</h3>
	<p>
		<label for="use-styles">
			<input type="checkbox" name="use-styles" value="1" <?= $useStylesChecked ?> id="use-styles">
			Use the built in plugin styles.
		</label>
		<br>
		<i>Uncheck this if you plan using your own styles and don't want to override the built-in plugin styling.</i>
	</p>
	<p>
		<label for="open-results-in-new-window">
			<input type="checkbox" name="open-results-in-new-window" id="open-results-in-new-window" value="1" <?= $openResultsInNewWindowChecked ?>>
			Open the search results in a new window
		</label>
	</p>
	<p>
		<label for="display-title">
			<input type="checkbox" name="display-title" id="display-title" value="1" <?= $displayTitleChecked ?>>
			Display the search box title
		</label>
	</p>
	<p>
		<label for="hide-options-until-focus">
			<input type="checkbox" name="hide-options-until-focus" id="hide-options-until-focus" value="1" <?= $hideOptionsUntilFocusChecked ?>>
			Hide all search options until the input search box is focused.
		</label>
	</p>
	<p>
		<label class="block">Placeholder text</label>
		<i>The text that shows in the search input box when not focused</i>
		<br>
		<input type="text" name="input-placeholder" value="<?= esc_attr($inputPlaceholder) ?>">
	</p>

	<?php submit_button( "Save", 'primary', 'lms-settings' ) ?>

</form>
</div>